#!/usr/bin/env python3

"""Helper functions for working with common filetypes and functions."""

import time
import json
import csv

from pathlib import Path


def module_name(File: str = __file__) -> str:
    """Get filename without any quotes, special characters, or path
    information from the system.
    Takes '__file__' from the calling module as an argument."""
    module_name = File.strip("./\\'\" .py")
    if '/' in module_name:
        module_name = module_name.split('/')[-1]
    elif '\\' in module_name:
        module_name = module_name.split('\\')[-1]
    return module_name


def countdown(Seconds: float = 5.0, Text: str = "{seconds}...") -> None:
    """Counts down from Seconds, alerting the user to the remaining
    seconds.
    Text must contain the substring '{seconds}'. If nothing
    is passed into Text, then just the remaining seconds are used."""
    for seconds in range(Seconds, 0, -1):
        print(Text.format(seconds=seconds))
        time.sleep(1.0)
    return None


def to_json(IterableObj: (list, tuple), savePath: (Path, str),
            Mode: str = 'w') -> None:
    """Writes an iterable to a JSON file using a human readable
    style. Raises an exception if unsuccessful."""
    savePath = Path(savePath)
    with open(savePath, Mode, encoding="utf-8") as json_out:
        json.dump(IterableObj, json_out, ensure_ascii=False, indent=4)
    return None


def dict_to_csv(Data: list, savePath: (Path, str), Mode: str = 'a') -> None:
    """Saves a list of dicts to CSV."""
    with open(savePath, Mode, newline='') as csv_out:
        csv_writer = csv.DictWriter(csv_out, fieldnames=Data[0].keys())
        csv_writer.writerows(Data)
    return None


def save_csv(IterableObj: (list, tuple, dict), savePath: (Path, str),
             Mode: str = 'w') -> None:
    """Writes an iterable to a CSV file. Raises an exception if
    unsuccessful."""
    with open(savePath, Mode, newline='') as csv_out:
        if isinstance(IterableObj, list) or isinstance(IterableObj, tuple):
            csv_writer = csv.writer(csv_out)
            csv_writer.writerow(IterableObj)
        elif isinstance(IterableObj, dict):
            csv_writer = csv.DictWriter(csv_out)
            csv_writer.writerows(IterableObj)
        else:
            raise TypeError("'save_csv' can only save a (list, tuple, dict)")

    return None


def load_json(loadPath: (Path, str)) -> (list, dict):
    """Returns a list or dict loaded from a JSON file specified by
    loadPath."""
    loadPath = Path(loadPath).expanduser().absolute()
    # Cast to Path, and ensure absolute path
    with open(loadPath, 'r') as json_in:
        return json.load(json_in)


def load_csv(loadPath: (Path, str)) -> list:
    """Returns a list loaded from a CSV file specified by
    loadPath."""
    loadPath = Path(loadPath).expanduser().absolute()
    # Cast to Path, and ensure absolute path
    with open(loadPath, 'r') as csv_in:
        reader = csv.reader(csv_in)  # Create CSV reader
        return list(reader)  # "[varname]" to remove outer list
